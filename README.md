# Android CameraX Demo : 实现预览/拍照/录制视频/图片分析/对焦/切换摄像头等操作

## 项目简介

本项目是一个基于Android CameraX的Demo，展示了如何使用CameraX库实现预览、拍照、录制视频、图片分析、对焦以及切换摄像头等功能。通过这个Demo，你可以快速了解CameraX的基本用法，并将其应用到自己的Android项目中。

## 功能列表

- **预览**：使用CameraX实现相机预览功能。
- **拍照**：通过CameraX拍摄照片并保存到本地。
- **录制视频**：使用CameraX录制视频并保存到本地。
- **图片分析**：利用CameraX进行实时图片分析，例如人脸检测、物体识别等。
- **对焦**：实现相机的自动对焦功能。
- **切换摄像头**：支持前后摄像头的切换。

## 使用说明

1. **克隆仓库**：
   ```bash
   git clone https://github.com/yourusername/CameraXDemo.git
   ```

2. **导入项目**：
   将项目导入到Android Studio中。

3. **运行项目**：
   连接Android设备或使用模拟器，运行项目即可体验CameraX的各种功能。

## 相关博客

更多关于CameraX的详细介绍和使用方法，请参考我的博客文章：
[Android CameraX Demo详解](https://blog.csdn.net/EthanCo/article/details/125603671)

## 贡献

如果你有任何改进建议或发现了Bug，欢迎提交Issue或Pull Request。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

希望这个Demo能帮助你更好地理解和使用Android CameraX！